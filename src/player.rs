use std::{
    cmp::Ordering,
    fmt::Debug,
    io::{self, BufRead, BufReader, Read, Write},
    process::Child,
};

use crate::card::{Card, ParseCardError};

use rand::Rng;
use thiserror::Error;

pub trait PlayerIoCore: Debug {
    fn reader(&mut self) -> &mut dyn Read;
    fn writer(&mut self) -> &mut dyn Write;
}

pub trait PlayerIo: Debug {
    fn welcome(&mut self) -> io::Result<()>;

    fn make_bid(&mut self, treasure: Card) -> Result<Result<Card, ParseCardError>, io::Error>;

    fn show_round_end(&mut self, outcome: Ordering, opponent_card: Card) -> io::Result<()>;
}

impl<T> PlayerIo for T
where
    T: PlayerIoCore,
{
    fn welcome(&mut self) -> io::Result<()> {
        writeln!(self.writer(), "Welcome to the game of pure strategy!")
    }

    fn make_bid(&mut self, treasure: Card) -> Result<Result<Card, ParseCardError>, io::Error> {
        writeln!(self.writer(), "On the table: {}", treasure)?;

        let mut line = String::new();
        BufReader::new(self.reader()).read_line(&mut line)?;
        let line = line.trim_end();
        Ok(line.parse::<Card>())
    }

    fn show_round_end(&mut self, outcome: Ordering, opponent_card: Card) -> io::Result<()> {
        // TODO: render outcome properly.
        writeln!(self.writer(), "{:?}", outcome)?;
        writeln!(self.writer(), "Opponent played: {}.", opponent_card)
    }
}

#[derive(Debug)]
struct ProcessPlayer {
    child: Child,
}

impl ProcessPlayer {
    // TODO: work out args here.
    pub fn new() -> io::Result<Self> {
        todo!()
    }
}

impl Drop for ProcessPlayer {
    fn drop(&mut self) {
        let _ = self.child.kill();
    }
}

impl PlayerIoCore for ProcessPlayer {
    fn reader(&mut self) -> &mut dyn Read {
        self.child
            .stdout
            .as_mut()
            .expect("We gave the child process a stdout.")
    }

    fn writer(&mut self) -> &mut dyn Write {
        self.child
            .stdin
            .as_mut()
            .expect("We gave the child a stdin.")
    }
}

#[derive(Debug)]
pub struct TextStreamPlayer<R, W> {
    reader: R,
    writer: W,
}

impl<R, W> TextStreamPlayer<R, W>
where
    R: Read,
    W: Write,
{
    pub fn new(reader: R, writer: W) -> Self {
        Self { reader, writer }
    }
}

impl<R, W> PlayerIoCore for TextStreamPlayer<R, W>
where
    R: Read + Debug,
    W: Write + Debug,
{
    fn reader(&mut self) -> &mut dyn Read {
        &mut self.reader
    }

    fn writer(&mut self) -> &mut dyn Write {
        &mut self.writer
    }
}

#[derive(Debug)]
pub struct RandomPlayerIo<R> {
    rng: R,
    hand: Vec<Card>,
}

impl<R> RandomPlayerIo<R>
where
    R: Rng,
{
    pub fn new(rng: R) -> Self {
        Self {
            rng,
            hand: Card::full_hand().collect(),
        }
    }
}

impl<R> PlayerIo for RandomPlayerIo<R>
where
    R: Rng + Debug,
{
    fn welcome(&mut self) -> io::Result<()> {
        Ok(())
    }

    fn make_bid(&mut self, _treasure: Card) -> io::Result<Result<Card, ParseCardError>> {
        Ok(self.hand.pop().ok_or(ParseCardError::Empty))
    }

    fn show_round_end(&mut self, _outcome: Ordering, _opponent_card: Card) -> io::Result<()> {
        Ok(())
    }
}

#[derive(Debug)]
pub struct Player {
    io: Box<dyn PlayerIo>,
    hand: Vec<Card>,
    hoarde: Vec<Card>,
}

#[derive(Debug, Error)]
#[error("Card not in hand: {0}")]
pub struct MissingCard(Card);

impl Player {
    pub fn new(io: Box<dyn PlayerIo>) -> Self {
        Self {
            io,
            hand: Card::full_hand().collect(),
            hoarde: Vec::new(),
        }
    }

    pub fn welcome(&mut self) -> io::Result<()> {
        self.io.welcome()
    }

    /// Shows the player what they're bidding for, reads ther bid and returns it,
    /// removing it from their hand.
    pub fn make_bid(
        &mut self,
        treasure: Card,
    ) -> Result<Result<Result<Card, MissingCard>, ParseCardError>, io::Error> {
        let bid = match self.io.make_bid(treasure)? {
            Ok(bid) => bid,
            Err(parse_error) => return Ok(Err(parse_error)),
        };

        Ok(Ok(self.take_card(bid)))
    }

    /// Returns whether the player has a specific card in their hand.
    pub fn has_card(&self, card: Card) -> bool {
        self.hand.iter().any(|in_hand| *in_hand == card)
    }

    /// Tries to take a specific card from the hand.
    /// Removes the card and returns it if present,
    /// returns `None` if not.
    pub fn take_card(&mut self, card: Card) -> Result<Card, MissingCard> {
        let i = self
            .hand
            .iter()
            .position(|in_hand| *in_hand == card)
            .ok_or(MissingCard(card))?;
        Ok(self.hand.swap_remove(i))
    }

    pub fn show_round_end(&mut self, outcome: Ordering, opponent_card: Card) -> io::Result<()> {
        self.io.show_round_end(outcome, opponent_card)
    }

    pub fn hoarde_value(&self) -> i32 {
        self.hoarde
            .iter()
            .copied()
            .map(Card::value)
            .map(i32::from)
            .sum()
    }

    pub fn give_treasure(&mut self, treasure: Card) {
        self.hoarde.push(treasure);
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn card_ownership() {
        let mut player = Player::new(Box::new(TextStreamPlayer {
            reader: &[][..],
            writer: Vec::<u8>::new(),
        }));

        for c in Card::full_hand() {
            assert!(player.has_card(c));
        }

        player.take_card(Card::new(7).unwrap()).unwrap();
        assert!(!player.has_card(Card::new(7).unwrap()));
        assert!(player.has_card(Card::new(6).unwrap()));
    }
}
