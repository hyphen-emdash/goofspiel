use std::{
    fmt::{self, Display, Formatter},
    str::FromStr,
};

use thiserror::Error;

#[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct Card {
    value: u8,
    // suit doesn't matter
}

impl Card {
    pub fn new(value: u8) -> Option<Self> {
        if (1..=13).contains(&value) {
            Some(Self { value })
        } else {
            None
        }
    }

    /// Returns an iterator over all the possible values of cards.
    pub fn full_hand() -> impl Iterator<Item = Card> {
        (1..=13).filter_map(Card::new)
    }

    pub fn value(self) -> u8 {
        self.value
    }
}

impl Display for Card {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self.value {
            1 => write!(f, "A"),
            11 => write!(f, "J"),
            12 => write!(f, "Q"),
            13 => write!(f, "K"),
            n if (1..=10).contains(&n) => write!(f, "{}", n),
            _ => unreachable!("Cards can only have value ace through king."),
        }
    }
}

#[derive(Debug, Error)]
pub enum ParseCardError {
    #[error("Empty string")]
    Empty,
    #[error("Value out of range: ({0})")]
    OutOfRange(u8),
    #[error("Invalid card: '{0}'")]
    Malformed(String),
}

impl FromStr for Card {
    type Err = ParseCardError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "" => Err(ParseCardError::Empty),
            "A" => Ok(Card::new(1).unwrap()),
            "J" => Ok(Card::new(11).unwrap()),
            "Q" => Ok(Card::new(12).unwrap()),
            "K" => Ok(Card::new(13).unwrap()),

            s => {
                if let Ok(n) = u8::from_str(s) {
                    Card::new(n).ok_or(ParseCardError::OutOfRange(n))
                } else {
                    Err(ParseCardError::Malformed(s.to_owned()))
                }
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn valid() {
        for i in 1..=13 {
            let c = Card::new(i).expect("Value is bewteen 1 and 13");
            assert_eq!(c.value(), i);
        }
    }

    #[test]
    fn value_0() {
        assert!(Card::new(0).is_none());
    }

    #[test]
    fn value_high() {
        for i in 14..=u8::MAX {
            assert_eq!(Card::new(i), None);
        }
    }

    #[test]
    fn display_face() {
        let ace = Card::new(1).unwrap();
        let jack = Card::new(11).unwrap();
        let queen = Card::new(12).unwrap();
        let king = Card::new(13).unwrap();

        assert_eq!(ace.to_string(), "A");
        assert_eq!(jack.to_string(), "J");
        assert_eq!(queen.to_string(), "Q");
        assert_eq!(king.to_string(), "K");
    }

    #[test]
    fn display_num() {
        for i in 2..=10 {
            assert_eq!(Card::new(i).unwrap().to_string(), i.to_string());
        }
    }
}
