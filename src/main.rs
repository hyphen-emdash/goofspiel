mod card;
mod player;

use std::{cmp::Ordering, io};

use rand::prelude::*;
use squirrel_rng::SquirrelRng;

use crate::{
    card::Card,
    player::{Player, RandomPlayerIo, TextStreamPlayer},
};

// TODO: Have the players be controllable by subprocesses.
// TODO: Allow a seed to control order of treasure pile.

fn main() {
    if let Err(e) = run() {
        eprintln!("{}", e);
    }
}

fn run() -> io::Result<()> {
    let mut treasure: Vec<Card> = Card::full_hand().collect();
    let mut rng = SquirrelRng::with_seed(0);
    treasure.shuffle(&mut rng);

    // For now, both players go by keyboard.
    let mut players: [Player; 2] = [
        Player::new(Box::new(TextStreamPlayer::new(io::stdin(), io::stdout()))),
        Player::new(Box::new(RandomPlayerIo::new(SquirrelRng::with_seed(1)))),
    ];

    let outcome = play(&mut treasure, &mut players)?;
    let winner = ordering_to_winner(outcome);

    eprintln!("Winner: {:?}", winner);
    Ok(())
}

/// Has two players play against each other and returns the index of the winner.
fn play(treasure: &mut Vec<Card>, players: &mut [Player; 2]) -> io::Result<Ordering> {
    for player in players.iter_mut() {
        player.welcome()?;
    }

    while let Some(card) = treasure.pop() {
        // Collect bids.
        let mut bids: [Card; 2] = [Card::new(1).unwrap(); 2];
        for i in 0..2 {
            let bid = players[i].make_bid(card)?;
            bids[i] = match bid {
                Ok(Ok(bid)) => bid,
                _ => {
                    // If the player does not bid correctly, they automatically lose.
                    return Ok([Ordering::Less, Ordering::Greater][i]);
                }
            }
        }

        let outcome = bids[0].cmp(&bids[1]);

        // Compare.
        let round_winner = ordering_to_winner(outcome);

        // Give treasure to winner of round.
        if let Some(i) = round_winner {
            players[i].give_treasure(card);
        }

        // Let the players know what happened.
        players[0].show_round_end(outcome, bids[1])?;
        players[1].show_round_end(outcome.reverse(), bids[0])?;
    }

    Ok(players[0].hoarde_value().cmp(&players[1].hoarde_value()))
}

fn ordering_to_winner(outcome: Ordering) -> Option<usize> {
    match outcome {
        Ordering::Less => Some(1),
        Ordering::Equal => None,
        Ordering::Greater => Some(0),
    }
}
